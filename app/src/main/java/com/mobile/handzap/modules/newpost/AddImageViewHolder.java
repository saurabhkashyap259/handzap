package com.mobile.handzap.modules.newpost;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mobile.handzap.R;

import java.io.IOException;

public class AddImageViewHolder extends RecyclerView.ViewHolder {

    public ImageView addImageIV;
    private Context context;

    public AddImageViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        this.context = context;
        addImageIV = itemView.findViewById(R.id.item_add_image_iv);
    }

    public void setData(Uri uri) {
        if(uri != null) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                addImageIV.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

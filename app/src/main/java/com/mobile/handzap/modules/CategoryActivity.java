package com.mobile.handzap.modules;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mobile.handzap.R;
import com.mobile.handzap.modules.category.models.Category;
import com.mobile.handzap.modules.category.adapters.CategoryListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CategoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CategoryListAdapter adapter;
    private List<Category> categoryList = new ArrayList<>();
    private HashMap<Integer, Category> selectedCategoryMap = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        //Get the ids
        Toolbar toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.category_rv);

        //Toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Post Categories");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Recycler view
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new CategoryListAdapter(this, categoryList);
        recyclerView.setAdapter(adapter);

        //adapter
        adapter.setInterfaceCategoryAdapter(new CategoryListAdapter.InterfaceCategoryAdapter() {
            @Override
            public void onItemClick(Category category) {
                if(category.isSelected()) {
                    category.setSelected(false);
                    removeCategory(category);
                }else {
                    category.setSelected(true);
                    addCategory(category);
                }

                adapter.notifyDataSetChanged();
            }
        });

        getCategoryList();
    }

    private void getCategoryList() {
        for(int i=1; i<49; i++) {
            Category category = new Category();
            category.setId(i);
            category.setTitle("Category " + i);
            categoryList.add(category);
        }
        adapter.notifyDataSetChanged();
    }

    private void addCategory(Category category) {
        if(!selectedCategoryMap.containsKey(category.getId())) {
            selectedCategoryMap.put(category.getId(), category);
        }
    }

    private void removeCategory(Category category) {
        selectedCategoryMap.remove(category.getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_select) {
            finishActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishActivity() {
        Intent intent = new Intent();
        intent.putExtra("selected_category", selectedCategoryMap);
        setResult(RESULT_OK, intent);
        onBackPressed();
    }
}

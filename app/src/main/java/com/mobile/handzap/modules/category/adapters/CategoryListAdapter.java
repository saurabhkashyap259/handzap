package com.mobile.handzap.modules.category.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.handzap.R;
import com.mobile.handzap.modules.category.adapters.viewholders.CategoryListViewHolder;
import com.mobile.handzap.modules.category.models.Category;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListViewHolder> {

    private Context context;
    private List<Category> categoryList;
    private LayoutInflater layoutInflater;
    private InterfaceCategoryAdapter interfaceCategoryAdapter;

    public CategoryListAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_category, viewGroup, false);
        return new CategoryListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListViewHolder categoryListViewHolder, int i) {
        categoryListViewHolder.setData(categoryList.get(i), interfaceCategoryAdapter);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public interface InterfaceCategoryAdapter {
        void onItemClick(Category category);
    }

    public void setInterfaceCategoryAdapter(InterfaceCategoryAdapter listener) {
        this.interfaceCategoryAdapter = listener;
    }
}

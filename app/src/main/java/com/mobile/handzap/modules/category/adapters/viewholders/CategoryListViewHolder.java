package com.mobile.handzap.modules.category.adapters.viewholders;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.handzap.R;
import com.mobile.handzap.modules.category.adapters.CategoryListAdapter;
import com.mobile.handzap.modules.category.models.Category;

public class CategoryListViewHolder extends RecyclerView.ViewHolder {

    public ImageView categoryImageIV;
    public TextView categoryTitleTV;
    public ImageView categorySelectedIV;
    public ConstraintLayout rootLayout;

    public CategoryListViewHolder(@NonNull View itemView) {
        super(itemView);

        categoryImageIV = itemView.findViewById(R.id.item_category_image_iv);
        categoryTitleTV = itemView.findViewById(R.id.item_category_title_tv);
        categorySelectedIV = itemView.findViewById(R.id.item_category_selected_iv);
        rootLayout = itemView.findViewById(R.id.item_category_root_layout_cl);
    }

    public void setData(final Category category, final CategoryListAdapter.InterfaceCategoryAdapter interfaceCategoryAdapter) {
        if(category != null) {
            categoryTitleTV.setText(category.getTitle());
            if(category.isSelected()) {
                categorySelectedIV.setVisibility(View.VISIBLE);
            }else {
                categorySelectedIV.setVisibility(View.INVISIBLE);
            }

            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interfaceCategoryAdapter.onItemClick(category);
                }
            });
        }
    }
}

package com.mobile.handzap.modules.newpost.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.handzap.R;
import com.mobile.handzap.modules.newpost.AddImageViewHolder;

import java.util.List;

public class AddImageAdapter extends RecyclerView.Adapter<AddImageViewHolder> {

    private Context context;
    private List<Uri> uriList;

    public AddImageAdapter(Context context, List<Uri> uriList) {
        this.context = context;
        this.uriList = uriList;
    }

    @NonNull
    @Override
    public AddImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_add_image, viewGroup, false);
        return new AddImageViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(@NonNull AddImageViewHolder addImageViewHolder, int i) {
        addImageViewHolder.setData(uriList.get(i));
    }

    @Override
    public int getItemCount() {
        return uriList.size();
    }
}

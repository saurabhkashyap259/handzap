package com.mobile.handzap.modules;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mobile.handzap.R;
import com.mobile.handzap.modules.category.models.Category;
import com.mobile.handzap.modules.location.LocationActivity;
import com.mobile.handzap.modules.newpost.adapters.AddImageAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class NewPostActivity extends AppCompatActivity {

    private final int REQUEST_CODE_PICK_IMAGE = 40001;
    private final int REQUEST_CODE_PICK_PLACE = 40002;
    private final int REQUEST_CODE_PICK_CATEGORY = 40003;
    private TextView categoryTV, locationTV, rateTV, paymentTV, startDateTV, jobTermTV, titleCharCountTV, desCharCountTV;
    private TextInputEditText titleTIET, desTIET;
    private ImageView addIV;
    private RecyclerView imageRV;
    private AddImageAdapter addImageAdapter;
    private List<Uri> uriList = new ArrayList<>();

    private int selectedIndex = -1;
    private final Calendar c = Calendar.getInstance();

    private HashMap<Integer, Category> selectedCategoryMap = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        //Get the ids
        Toolbar toolbar = findViewById(R.id.toolbar);
        categoryTV = findViewById(R.id.new_post_category_tv);
        rateTV = findViewById(R.id.new_post_rate_tv);
        paymentTV = findViewById(R.id.new_post_payment_method_tv);
        locationTV = findViewById(R.id.new_post_location_tv);
        startDateTV = findViewById(R.id.new_post_start_date_tv);
        jobTermTV = findViewById(R.id.new_post_job_term_tv);
        addIV = findViewById(R.id.new_post_image_add_iv);
        imageRV = findViewById(R.id.new_post_image_rv);
        titleTIET = findViewById(R.id.new_post_title_tiet);
        desTIET = findViewById(R.id.new_post_des_tiet);
        titleCharCountTV = findViewById(R.id.new_post_title_char_count_tv);
        desCharCountTV = findViewById(R.id.new_post_des_char_count_tv);

        //Toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("New Post");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Add image
        imageRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        addImageAdapter = new AddImageAdapter(this, uriList);
        imageRV.setAdapter(addImageAdapter);

        //Title
        titleTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int left = 50 - s.length();
                if(left > -1)
                    titleCharCountTV.setText(left + " characters left");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Description
        desTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int left = 300 - s.length();
                if(left > -1)
                    desCharCountTV.setText(left + " characters left");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Category
        categoryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCategoryActivity();
            }
        });

        //Add image
        addIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagePickerActivity();
            }
        });

        //Location
        locationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPlacePickerActivity();
            }
        });

        //Rate
        rateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRateDialog();
            }
        });

        //Payment
        paymentTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaymentDialog();
            }
        });

        //Start date
        startDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStartDateDialog();
            }
        });

        //job term
        jobTermTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openJobTermDialog();
            }
        });
    }

    private void openCategoryActivity() {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivityForResult(intent, REQUEST_CODE_PICK_CATEGORY);
    }

    private void openImagePickerActivity() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, REQUEST_CODE_PICK_IMAGE);
    }

    private void openLocationActivity() {
        Intent intent = new Intent(this, LocationActivity.class);
        startActivity(intent);
    }

    private void openPlacePickerActivity() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), REQUEST_CODE_PICK_PLACE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void openRateDialog() {
        final CharSequence[] items = {" No Preference "," Fixed Budget "," Hourly Rate "};
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Choose your version");
        b.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        });
        b.setPositiveButton("SELECT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(selectedIndex != -1)
                    rateTV.setText(items[selectedIndex]);
            }
        });
        b.setNegativeButton("CANCEL", null);
        AlertDialog d = b.create();
        d.show();
    }

    private void openPaymentDialog() {
        final CharSequence[] items = {" No Preference "," E-Payment "," Cash "};
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Choose your version");
        b.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        });
        b.setPositiveButton("SELECT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(selectedIndex != -1)
                    paymentTV.setText(items[selectedIndex]);
            }
        });
        b.setNegativeButton("CANCEL", null);
        AlertDialog d = b.create();
        d.show();
    }

    private void openStartDateDialog() {
        Dialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
                .get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                datePicked(((DatePickerDialog) dialog).getDatePicker().getYear(),
                        ((DatePickerDialog) dialog).getDatePicker().getMonth(), ((DatePickerDialog) dialog)
                                .getDatePicker().getDayOfMonth());
            }
        });
        datePickerDialog.show();
    }

    private void datePicked(int year, int month, int day) {
        startDateTV.setText(String.valueOf(year) + "/" +
                String.valueOf(month) + "/" + String.valueOf(day));
    }

    private void openJobTermDialog() {
        final CharSequence[] items = {" Recurring Job "," Same Day Job "," Multi Days Job "};
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Choose your version");
        b.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        });
        b.setPositiveButton("SELECT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(selectedIndex != -1)
                    jobTermTV.setText(items[selectedIndex]);
            }
        });
        b.setNegativeButton("CANCEL", null);
        AlertDialog d = b.create();
        d.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_PICK_IMAGE:
                    if(data != null && data.getData() != null) {
                        Uri uri = data.getData();
                        uriList.add(uri);
                        addImageAdapter.notifyDataSetChanged();
                    }
                    break;

                case REQUEST_CODE_PICK_PLACE:
                    Place place = PlacePicker.getPlace(data, this);
                    locationTV.setText(place.getName());
                    break;

                case REQUEST_CODE_PICK_CATEGORY:
                    if(data != null && data.getSerializableExtra("selected_category") != null) {
                        selectedCategoryMap.clear();
                        selectedCategoryMap.putAll((Map<? extends Integer, ? extends Category>) data.getSerializableExtra("selected_category"));
                        categoryTV.setText(selectedCategoryMap.size() + " categories selected");
                    }
                    break;

            }
        }
    }
}
